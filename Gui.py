import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
#from tkinter import messagebox
from Sender import Sender
from Receiver import Receiver
from Channel import Channel
from ConstellationPoints import getConstPoints
from random import getrandbits

class GUI(object):
    """Class for the GUI of the Programm"""
    
    def BGraph(self):
        # Hier Methode für Graph
        tk.messagebox.showinfo("B", "B2")

    def transmit(self):
        # creates random string of bits and sends it using sender, channel and receiver objects.
        self.sender.resetAll()
        self.receiver.resetAll()
        metadata = {"errorsOccured": 0}

        # Generate random n bit message
        bits = ""
        for _ in range(256):
            if getrandbits(1):
                bits += "1"
            else:
                bits += "0"
        self.sender.addBitsToBuffer(bits)        

        while self.sender.canSend():
            signal  = self.sender.generateSignal()
            signal_ = self.channel.sendSignal(signal)
            self.receiver.receiveSignal(signal_)
            # detect and log total number of bit errors
            for bitSent, bitReceived in zip(self.sender.tmpSymbol, self.receiver.predictedSymbol):
                if bitSent is not bitReceived:
                    metadata["errorsOccured"] += 1

        # Show metadata information
        #print("Sender metadata:   " + str(self.sender.metadata))
        #print("Receiver metadata: " + str(self.receiver.metadata))
        #print("General metadata:  " + str(metadata))

        self.hist_errorsOccurred.append(metadata["errorsOccured"] / self.sender.metadata["bitsSent"] *100)
        self.hist_errorsOccurred.pop(0)
        self.hist_packetsCorrected.append(self.receiver.metadata["packetsCorrected"] / self.receiver.metadata["numPacketsEC"] *100)
        self.hist_packetsCorrected.pop(0)
        self.hist_corruptPacketsDetected.append(self.receiver.metadata["corruptPacketsDetected"] / self.receiver.metadata["numPacketsED"] *100)
        self.hist_corruptPacketsDetected.pop(0)

        self.line1.set_ydata(self.hist_errorsOccurred)
        self.line2.set_ydata(self.hist_packetsCorrected)
        self.line3.set_ydata(self.hist_corruptPacketsDetected)
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def toggle(self):
        # Toggle running variable which decides if data is transmitted continuously.
        if self.isRunning is True:
            self.isRunning = False
        else:
            self.isRunning = True
            self.loop()

    def loop(self):
        """
        Calls itself recursively using window event handler as long as isRunning is True
        """
        self.transmit()
        if self.isRunning:
            self.window.after(0, self.loop)

    def DropDownEvent(self, event):
        """
        Is called when a drop down is modified. Updates all parameter dictionaries.
        """
        self.sender.setParam({"errorDetection": self.var_errorDetection.get(), "errorCorrection": self.var_errorCorrection.get(), "packetSizeEC": 4, "packetSizeED": 4, "constPoints": getConstPoints(self.var_ConstPoints.get())})
        self.receiver.setParam({"errorDetection": self.var_errorDetection.get(), "errorCorrection": self.var_errorCorrection.get(), "packetSizeEC": 4, "packetSizeED": 4, "constPoints": getConstPoints(self.var_ConstPoints.get())})
        self.channel.setParam({"errorType": self.var_errorType.get(), "standardDerivation":self.Slider1.get(), "standardDerivationBurst": self.Slider2.get(), "burstStopChance": 0.05, "burstStartChance": 0.01, "burstActive": False})
        

    def SliderEvent(self, value):
        """
        Is called when the Slider value is modified
        """
        self.channel.setParam({"errorType": self.var_errorType.get(), "standardDerivation": self.Slider1.get(), "standardDerivationBurst": self.Slider2.get(), "burstStopChance": 0.05, "burstStartChance": 0.01, "burstActive": False})
        
    # Init_function for class
    def __init__(self):
        # Set Variables for window
        self.window = tk.Tk()
        self.window.title('DataTransmission')
        self.window.geometry('700x740')

        #Frame
        self.TopTopFrame = tk.Frame(self.window)
        self.TopFrame = tk.Frame(self.window)
        self.BottomFrame = tk.Frame(self.window)
        self.TopTopFrame.grid(row= 0)
        self.TopFrame.grid(row=1)
        self.BottomFrame.grid(row=2)

        # Labels
        self.Label1 = tk.Label(self.TopTopFrame, text="PSK:         ")
        self.Label2 = tk.Label(self.TopTopFrame, text="     FEC:         ")
        self.Label3 = tk.Label(self.TopTopFrame, text="       FED:         ")
        self.Label4 = tk.Label(self.TopTopFrame, text="       ERT:           ")
        self.Label5 = tk.Label(self.TopTopFrame, text="    RND:     ")
        self.Label6 = tk.Label(self.TopTopFrame, text="                  BST:           ")

        self.Label1.pack(side = tk.LEFT);
        self.Label2.pack(side = tk.LEFT);
        self.Label3.pack(side = tk.LEFT);
        self.Label4.pack(side = tk.LEFT);
        self.Label5.pack(side = tk.LEFT);
        self.Label6.pack(side = tk.LEFT);
        
        # Text
        
        # Controls 
        self.Button_run = tk.Button(self.TopFrame, text ="RUN", command = self.toggle)
        self.Button_run.pack(side = tk.LEFT)

        # Other Variables
        self.isRunning = False
        self.hist_length = 100
        self.hist_xAxis = [x for x in range(-100 , 0, 1)]
        self.hist_corruptPacketsDetected = [0] * self.hist_length
        self.hist_packetsCorrected = [0] * self.hist_length
        self.hist_errorsOccurred = [0] * self.hist_length


        # DropDown
        self.var_ConstPoints = tk.StringVar(self.window)
        self.var_ConstPoints.set("BPSK")
        self.DropDown1 = tk.OptionMenu(self.TopFrame, self.var_ConstPoints, *["BPSK", "QPSK", "16-QAM-binary", "16-QAM-graycode"], command=self.DropDownEvent)
        self.DropDown1.pack(side = tk.LEFT)

        self.var_errorCorrection = tk.StringVar(self.window)
        self.var_errorCorrection.set("None")
        self.DropDown3 = tk.OptionMenu(self.TopFrame, self.var_errorCorrection, *["None", "HammingCode74", "ConvolutionalCode"], command=self.DropDownEvent)
        self.DropDown3.pack(side = tk.LEFT)

        self.var_errorDetection = tk.StringVar(self.window)
        self.var_errorDetection.set("None")
        self.DropDown2 = tk.OptionMenu(self.TopFrame, self.var_errorDetection, *["None", "Parity", "CRC"], command=self.DropDownEvent)
        self.DropDown2.pack(side = tk.LEFT)

        self.var_errorType = tk.StringVar(self.window)
        self.var_errorType.set("None")
        self.DropDown4 = tk.OptionMenu(self.TopFrame, self.var_errorType, *["None", "Random", "Burst"], command=self.DropDownEvent)
        self.DropDown4.pack(side = tk.LEFT)

        #Slider
        self.Slider1 = tk.Scale(self.TopFrame, from_=0, to=0.5, orient=tk.HORIZONTAL, command=self.SliderEvent, resolution=0.01)
        self.Slider1.set(0.25)
       # self.Label1.pack(side = tk.LEFT)
        self.Slider1.pack(side = tk.LEFT)

        self.Slider2 = tk.Scale(self.TopFrame, from_=0, to=3, orient=tk.HORIZONTAL, command=self.SliderEvent, resolution=0.1)
        self.Slider2.set(1.5)
       # self.Label2.pack(side = tk.LEFT)
        self.Slider2.pack(side = tk.LEFT)

        #Graphen
        self.figure = Figure(figsize=(7,7), dpi=100)
        self.ax1 = self.figure.add_subplot(311)
        self.ax2 = self.figure.add_subplot(312)
        self.ax3 = self.figure.add_subplot(313)

        self.ax1.set_xlim([-100, 0])
        self.ax2.set_xlim([-100, 0])
        self.ax3.set_xlim([-100, 0])
        
        self.ax1.set_ylim([0, 100])
        self.ax2.set_ylim([0, 100])
        self.ax3.set_ylim([0, 100])

        #Set data
        self.ax1.set_xlabel('Iterations')
        self.ax1.set_ylabel('Bit errors [%]')
        self.line1, = self.ax1.plot(self.hist_xAxis, self.hist_errorsOccurred)
    
        self.ax2.set_xlabel('Iterations')
        self.ax2.set_ylabel('Corrected invalid\n packets [%]')
        self.line2, = self.ax2.plot(self.hist_xAxis, self.hist_packetsCorrected)

        self.ax3.set_xlabel('Iterations')
        self.ax3.set_ylabel('Detected invalid\n packets [%]')
        self.line3, = self.ax3.plot(self.hist_xAxis, self.hist_corruptPacketsDetected)

        # Create and draw canvas
        self.canvas = FigureCanvasTkAgg(self.figure, self.BottomFrame)
        self.canvas.draw()

       # self.canvas.get_tk_widget().grid(row=5, column=1)
        self.canvas.get_tk_widget().pack(side = tk.TOP)

        # Create objects
        self.sender = Sender()
        self.receiver = Receiver()
        self.channel = Channel()

        # Manually call DropDownEvent Function to initially set all parameters.
        self.DropDownEvent(None)
