
def getConstPoints(constPointsType):
    if constPointsType == "BPSK":
        return { "1": [ 1, 0],
                 "0": [-1, 0]}

    elif constPointsType == "QPSK":
        return { "00": [ 1, 1],
                 "01": [-1, 1],
                 "10": [-1,-1],
                 "11": [ 1,-1] }

    elif constPointsType == "16-QAM-binary":
        constPoints = { "0000": [],
                    "0001": [],
                    "0010": [],
                    "0011": [],
                    "0100": [],
                    "0101": [],
                    "0110": [],
                    "0111": [],
                    "1000": [],
                    "1001": [],
                    "1010": [],
                    "1011": [],
                    "1100": [],
                    "1101": [],
                    "1110": [],
                    "1111": [] }
        cnt = 0
        for x in range(4):
            for y in range(4):
                cntBin = "{0:04b}".format(cnt)
                constPoints[cntBin] = [x*2/3 - 1, y*2/3 - 1]
                cnt+=1
        return constPoints

    elif constPointsType == "16-QAM-graycode":
        """
        Constellation point symbol mapping is performed according to grey code. 
        Neighbor symbols always have only one bit flipped --> less overall bit flips --> easier to correct errors
        """
        order = [2, 6, 14, 10, 3, 7, 15, 11, 1, 5, 13, 9, 0, 4, 12, 8]
        constPoints = { "0000": [],
                    "0001": [],
                    "0010": [],
                    "0011": [],
                    "0100": [],
                    "0101": [],
                    "0110": [],
                    "0111": [],
                    "1000": [],
                    "1001": [],
                    "1010": [],
                    "1011": [],
                    "1100": [],
                    "1101": [],
                    "1110": [],
                    "1111": [] }
        cnt = 0
        for x in range(4):
            for y in range(4):
                cntBin = "{0:04b}".format(order[cnt])
                constPoints[cntBin] = [x*2/3 - 1, y*2/3 - 1]
                cnt+=1
        return constPoints

    else:
        print("Invalid constellation point type specified.")
        return None