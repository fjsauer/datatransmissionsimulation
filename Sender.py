import numpy as np

class Sender:
    """
    Sender class which handles data transfer between buffers and generates symbols to send them over a channel.
    Class can partition its data buffers content into error detection and correction buffers to apply correction and detection algorithms.
    Encoded data in the error correction buffer can be passed to the channel class as a constellation point. 
    """
    def __init__(self):
        self.bufferData = ""
        self.bufferED = ""
        self.bufferEC = ""
        self.symbolLength = None
        self.param = None
        self.funcErrorDetection = None
        self.funcErrorCorrection = None
        self.tmpSymbol = ""
        self.metadata = {"bitsOverhead": 0,
                         "bitsPayload": 0,
                         "symbolsSent": 0,
                         "bitsSent": 0}

        # HammingCode(7,4)
        self.hamming74encoder = np.array([[1, 1, 0, 1], \
                                          [1, 0, 1, 1], \
                                          [1, 0, 0, 0], \
                                          [0, 1, 1, 1], \
                                          [0, 1, 0, 0], \
                                          [0, 0, 1, 0], \
                                          [0, 0, 0, 1]])

        # ConvolutionalCode(2 bit state machine)
        self.convCodeShiftRegister = ["0", "0"]
        self.convCodeCnt = 0

            

    def setParam(self, param):
        """
        Sets parameters for the sender. All parameters have to be provided as a dictionary.
        """
        self.param = param
        
        # extracts symbol length (of first symbol) from constellation point dictionary
        for key in self.param["constPoints"]:
            self.symbolLength = len(key)
            break

        # Setting function pointer for error detection method.
        if param["errorDetection"] == "None":
            self.funcErrorDetection = Sender.funcErrorDetectionNone
            self.payloadED = 16 # + 0 redundancy bits
        elif param["errorDetection"] == "Parity":
            self.funcErrorDetection = Sender.funcErrorDetectionParity
            self.payloadED = 15 # + 1 redundancy bits
        elif param["errorDetection"] == "CRC":
            self.funcErrorDetection = Sender.funcErrorDetectionCRC
            self.payloadED = 8 # + 8 redundancy bits
        else:
            print("[Error]: Invalid error detection method specified for sender.")

        # Setting function pointer for error correction method.
        if param["errorCorrection"] == "None":
            self.funcErrorCorrection = Sender.funcErrorCorrectionNone
            self.payloadEC = self.param["packetSizeEC"]
        elif param["errorCorrection"] == "HammingCode74":
            self.funcErrorCorrection = Sender.funcErrorCorrectionHammingCode
            self.param["packetSizeEC"] = 7
            self.payloadEC = 4
        elif param["errorCorrection"] == "ConvolutionalCode":
            self.funcErrorCorrection = Sender.funcErrorCorrectionConvolutionalCode
            self.param["packetSizeEC"] = 2
            self.payloadEC = 1
        else:
            print("[Error]: Invalid error correction method specified for sender.")


    def addBitsToBuffer(self, bitstream):
        """
        Adds bits to the dataBuffer. Bits have to be provided as a string of '0' and '1' characters.
        """
        self.bufferData += bitstream

    
    def canSend(self):
        """
        Checks if there is enough data to send in a potential next function call.
        """
        if len(self.bufferEC) >= self.symbolLength or len(self.bufferED) >= self.payloadEC or len(self.bufferData) >= self.payloadED:
            return True
        else: 
            return False

    def encodePacket(self):
        """
        Checks if buffers are empty and the next buffer in sequence has enough bits to fill the needed payload.
        If yes, bits are transfered from one to the next buffer and the specified redundancy function (for detection or correction) is called.
        """
        if len(self.bufferED) < self.payloadEC and len(self.bufferData) >= self.payloadED:
            # Move data of size payloadED from data buffer to error detection buffer.
            self.bufferED += self.funcErrorDetection(self, self.bufferData[:self.payloadED])
            self.bufferData = self.bufferData[self.payloadED:]

        if len(self.bufferEC) < self.symbolLength and len(self.bufferED) >= self.payloadEC:
            # Move data of size payloadEC from error detection buffer to error correction buffer.
            self.bufferEC += self.funcErrorCorrection(self, self.bufferED[:self.payloadEC])
            self.bufferED = self.bufferED[self.payloadEC:]
        

    def resetAll(self):
        """
        Clears all buffers and shift register.
        """
        self.bufferData = ""
        self.bufferED = ""
        self.bufferEC = ""

        self.convCodeCnt = 0
        self.convCodeShiftRegister = ["0", "0"]

        self.metadata = {"bitsOverhead": 0,
                         "bitsPayload": 0,
                         "symbolsSent": 0,
                         "bitsSent": 0}


    def generateSignal(self):
        """
        Checks if there are enough bits in the error correction buffer to create one symbol.
        If yes, a symbol is created according to the specified constellation diagram. 
        All bits which are represented by the newly created symol are being removed from the error correction buffer.
        If there is not enough bits in the error correction buffer the method returns 'None'.
        """
        self.encodePacket()
        if len(self.bufferEC) >= self.symbolLength:
            self.tmpSymbol = self.bufferEC[:self.symbolLength] 
            self.bufferEC = self.bufferEC[self.symbolLength:]
            self.metadata["symbolsSent"] += 1
            self.metadata["bitsSent"] += self.symbolLength
            return self.param["constPoints"][self.tmpSymbol]
        return None


    def funcErrorDetectionNone(self, bits):
        """
        Input bits are passed without modification.
        """
        return bits


    def funcErrorDetectionParity(self, bits):
        """
        Add a parity bit to the end of bitsIn and return.
        """
        counter = 0
        for bit in bits:
                if bit == "1":
                    counter += 1
        if counter % 2 == 0:
            bits += "0"
        else:
            bits += "1"
        return bits
    

    def funcErrorDetectionCRC(self, bits):
        # to be implemented
        # TODO: Tim -> define sender.CRC
        generator = "100011101"
        bitlen = len(bits)
        genlen = len(generator) #genlen von CRC 8 ist 9
        curBit = 0
        # nothing = "yield from" # for reference
        bitCalc = list(bits)
        for i in range(genlen-1):
            bitCalc += "0"
        while curBit < bitlen:
            #print("".join(bitCalc))
            if bitCalc[curBit] == "1":
                for i in range(genlen):
                    #print(len(bitCalc), curBit+i)
                    bitCalc[curBit+i] = self.XOR(bitCalc[curBit+i], generator[i])
            curBit+=1
            #print(curBit, bitlen)
        for i in range(genlen-1):
            bits += bitCalc[bitlen+i]
        return bits


    def funcErrorCorrectionNone(self, bits):
        """
        Returns input bits without any modification.
        """
        return bits


    def funcErrorCorrectionHammingCode(self, bits):
        '''
        Implementation of HammingCode(7,4).
        Takes 4 bits of data as string and generates 7 bits of data multiplying with encoder matrix specified as class member variable.
        Returns 7 bits of data.
        '''
        message = np.array([list(bits)], dtype=int)
        encodedMessage = np.mod(np.dot(self.hamming74encoder, message.T), 2)
        return ''.join(str(x[0]) for x in encodedMessage)

    @staticmethod
    def XOR(bit1, bit2):
        if (bit1 == bit2):
            return "0"
        else:
            return "1"
    
    def funcErrorCorrectionConvolutionalCode(self, bit):
        """
        Takes one payload bit.
        Returns two bits calculated using a shift register of size 2. 
        """
        
        outBits = "".join([self.XOR(bit, self.convCodeShiftRegister[1]), self.XOR(self.XOR(bit, self.convCodeShiftRegister[0]), self.convCodeShiftRegister[1])])
        self.convCodeShiftRegister[1] = self.convCodeShiftRegister[0]
        self.convCodeShiftRegister[0] = bit

        # Reset state machine and counter after 4 function calls.
        self.convCodeCnt += 1
        if self.convCodeCnt > 3:
            self.convCodeShiftRegister = ["0", "0"]
            self.convCodeCnt = 0
        
        return outBits


def main():
    sender = Sender()

    bits = "0110"
    bitsSent = ""
    for bit in bits:
       bitsSent += sender.funcErrorCorrectionConvolutionalCode(bit)
    
    print("Bits sent: " + bitsSent)

    # print(len(sender.funcErrorDetectionCRC("11111111")))
    # sender.convCodeShiftRegister = ["0", "0"]
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # sender.convCodeShiftRegister = ["0", "0"]
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))

    # sender.convCodeShiftRegister = ["1", "0"]
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # sender.convCodeShiftRegister = ["1", "0"]
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))

    # sender.convCodeShiftRegister = ["0", "1"]
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # sender.convCodeShiftRegister = ["0", "1"]
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))

    # sender.convCodeShiftRegister = ["1", "1"]
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # sender.convCodeShiftRegister = ["1", "1"]
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))

    # Testing Shift Register implementation
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # print(sender.funcErrorCorrectionConvolutionalCode("1"))
    # print(sender.funcErrorCorrectionConvolutionalCode("0"))
    # print(sender.funcErrorDetectionCRC("11000010"))
    

if __name__ == "__main__":
    main()