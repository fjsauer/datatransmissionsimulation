import random as rd
import numpy as np


class Channel:
    """
    Channel class takes constellation point coordinates and modifies them
    according to the specified error function.
    The class also holds all implemented error functions.
    """
    def __init__(self):
        self.funcError = None
        self.param = None


    def setParam(self, param):
        """
        Sets parameters for the channel. All parameters have to be provided as a dictionary.
        """
        self.param = param
        if param["errorType"] == "None":
            self.funcError = Channel.errorFuncNoNoise
        elif param["errorType"] == "Random":
            self.funcError = Channel.errorFuncRandomNoise
        elif param["errorType"] == "Burst":
            self.funcError = Channel.errorFuncBurstNoise
        else:
            print("[Error]: Invalid error function specified.")


    def sendSignal(self, signalIn):
        return self.funcError(self, signalIn)


    def errorFuncNoNoise(self, inSignal):
        """
        No error is being added to the input constellation point coordinates.
        The point is being returned without any modification.
        """
        return inSignal


    def errorFuncRandomNoise(self, inSignal):
        """
        A noise value with a maximum amplitude specified in the parameters dictionary as maxNoise is being added
        to both coordinates of the input constellation point.
        The modfied constellation point coordinates are being returned. 
        """
        if inSignal is not None:
            outSignal = [inSignal[0] + np.random.normal(0, self.param["standardDerivation"]),
                        inSignal[1] + np.random.normal(0, self.param["standardDerivation"])]
            return outSignal
        else:
            return None


    def errorFuncBurstNoise(self, inSignal):
        """
        A noise value with a maximum amplitude depending on the current error burst status is being added
        to both coordinates of the input constellation point.
        Depending on the specified chances the current burst status is being modified.
        The modfied constellation point coordinates are being returned. 
        """
        if inSignal is not None:
            if self.param["burstActive"] == True:
                outSignal = [inSignal[0] + rd.uniform(0, self.param["standardDerivationBurst"]),
                            inSignal[1] + rd.uniform(0, self.param["standardDerivationBurst"])]
                if rd.uniform(0, 1) < self.param["burstStopChance"]:
                    self.param["burstActive"] = False
            else:
                outSignal = [inSignal[0] + np.random.normal(0, self.param["standardDerivation"]),
                            inSignal[1] + np.random.normal(0, self.param["standardDerivation"])]
                if rd.uniform(0, 1) < self.param["burstStartChance"]:
                    self.param["burstActive"] = True
            return outSignal
        else:
            return None