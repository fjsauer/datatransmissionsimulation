bitstring = "110101"
generator = "101"


def base2DivisionEqualsZero(divident, divisor):
    remainder = int(divident, 2) % int(divisor, 2)
    print("Division: " + str(int(divident, 2)) + " % " + str(int(divisor, 2)) + " = " + str(remainder))
    if remainder == 0:
        return True
    else:
        return False

print(base2DivisionEqualsZero(bitstring, generator))