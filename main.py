from Sender import Sender
from Receiver import Receiver
from Channel import Channel
from Gui import GUI

def main():
    
    application = GUI()
    application.window.mainloop()

if __name__ == "__main__":
    main()