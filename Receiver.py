import numpy as np

class Receiver:
    """
    Receiver class which takes constellation point coordinates and reinterprets them as symbols
    according to the specified constellation diagram. Received bits are being stored into an error correction buffer.
    If the specified error correction packet size is reached, the specified error correction algorithm is applied to the buffer.
    The result is passed to the error detection buffer. If the specified error detection packet size is reached, the specified error detection algorithm
    is appleid to the data and its content is transfered into the data buffer.
    """
    def __init__(self):
        self.param = None
        self.bufferData = ""
        self.bufferED = ""
        self.bufferEC = ""
        self.funcErrorDetection = None
        self.funcErrorCorrection = None
        self.predictedSymbol = ""
        self.metadata = {"corruptPacketsDetected": 0,
                         "packetsCorrected": 0,
                         "numPacketsED": 0,
                         "numPacketsEC": 0}

        # HammingCode(7,4)
        self.hamming74decoder = np.array([[0, 0, 1, 0, 0, 0, 0], \
                                          [0, 0, 0, 0, 1, 0, 0], \
                                          [0, 0, 0, 0, 0, 1, 0], \
                                          [0, 0, 0, 0, 0, 0, 1]])

        self.hamming74checker = np.array([[1 ,0 ,1 ,0 ,1 ,0 ,1], \
                                          [0 ,1 ,1 ,0 ,0 ,1, 1], \
                                          [0, 0, 0, 1, 1, 1, 1]]) 

        # ConvolutionalCode(2 bit state machine)
        self.convCodeStates = [[[0, "00"], [2, "11"]],
                               [[0, "11"], [2, "00"]],
                               [[1, "01"], [3, "10"]],
                               [[1, "10"], [3, "01"]]]

        self.convCodeDecoder = [["0", "1", "", ""],
                                ["", "", "0", "1"],
                                ["0", "1", "", ""],
                                ["", "", "0", "1"]]

        self.convCodeDecoderRaw = [["00", "11", "", ""],
                                   ["", "", "01", "10"],
                                   ["11", "00", "", ""],
                                   ["", "", "10", "01"]]

        self.convCodeCosts = np.full((4, 6), np.inf)
        self.convCodeOrigin = np.ones((4, 6)) * -1
        self.convCodeCosts[0, 0] = 0


    def setParam(self, param):
        """
        Sets parameters for the receiver. All parameters have to be provided as a dictionary.
        """
        self.param = param # Some parameter may be overwritten for a selected error detection or correction method.

        # Setting function pointer for error detection method.
        if param["errorDetection"] == "None":
            self.funcErrorDetection = Receiver.funcErrorDetectionNone
            self.param["packetSizeED"] = 16
        elif param["errorDetection"] == "Parity":
            self.funcErrorDetection = Receiver.funcErrorDetectionParity
            self.param["packetSizeED"] = 16
        elif param["errorDetection"] == "CRC":
            self.funcErrorDetection = Receiver.funcErrorDetectionCRC
            self.param["packetSizeED"] = 16
        else:
            print("[Error]: Invalid redundancy function specified for receiver.")

        # Setting function pointer for error correction method.
        if param["errorCorrection"] == "None":
            self.funcErrorCorrection = Receiver.funcErrorCorrectionNone
        elif param["errorCorrection"] == "HammingCode74":
            self.funcErrorCorrection = Receiver.funcErrorCorrectionHammingCode
            self.param["packetSizeEC"] = 7
        elif param["errorCorrection"] == "ConvolutionalCode":
            self.funcErrorCorrection = Receiver.funcErrorCorrectionConvolutionalCode
            self.param["packetSizeEC"] = 8
        else:
            print("[Error]: Invalid error correction method specified for sender.")


    def decodePacket(self):
        """
        Checks wheather the error correction buffer contains a complete packet based on its size.
        If yes, the packet is analyzed and modified by the specified error correction method.
        The result is appended to toe error detection buffer.
        If the error detection buffer contains a complete packet the specified error detection method is applied on the buffer data.
        All remaining bits in the error detection buffer are transferred into the data buffer (this step is independent of the result of the error detection algorithm)
        """
        if len(self.bufferEC) >= self.param["packetSizeEC"]:
            self.bufferED += self.funcErrorCorrection(self, self.bufferEC[:self.param["packetSizeEC"]])
            self.bufferEC = self.bufferEC[self.param["packetSizeEC"]:]
            self.metadata["numPacketsEC"] += 1

        if len(self.bufferED) >= self.param["packetSizeED"]:
            self.bufferData += self.funcErrorDetection(self, self.bufferED[:self.param["packetSizeED"]])
            self.bufferED = self.bufferED[self.param["packetSizeED"]:]
            self.metadata["numPacketsED"] += 1

    def resetAll(self):
        self.bufferData = ""
        self.bufferEC = ""
        self.bufferED = ""
        self.metadata = {"corruptPacketsDetected": 0,
                         "packetsCorrected": 0,
                         "numPacketsED": 0,
                         "numPacketsEC": 0}

    def receiveSignal(self, signal):
        """
        This method takes a constellation  point and interprets it according to the specified constellation diagram.
        The constellation point with the smallest squared distance to to the received constellation point coordinates is choosen.
        The symbol corresponding to the choosen constellation point is added to the receivers error correction buffer as bits.
        After receiveing a symbol the decodePacket method is called to check if there is a full packet in either buffer .
        """
        if signal is not None:
            minSquaredDist = 99999
            self.predictedSymbol = ""
            for symbol in self.param["constPoints"]:
                squaredDist = (signal[0] - self.param["constPoints"][symbol][0]) * (signal[0] - self.param["constPoints"][symbol][0]) + \
                              (signal[1] - self.param["constPoints"][symbol][1]) * (signal[1] - self.param["constPoints"][symbol][1])
                if (squaredDist < minSquaredDist):
                    minSquaredDist = squaredDist
                    self.predictedSymbol = symbol
            self.bufferEC += self.predictedSymbol
        self.decodePacket()


    def funcErrorDetectionNone(self, bits):
        """
        Returns input bits without any modification.
        """
        return bits


    def funcErrorDetectionParity(self, bits):
        """
        Checks parity of input bits and decide if there was an error.
        Returns the input bits without the parity bit.
        """
        counter = 0
        for bit in bits:
                if bit == "1":
                    counter += 1
        if counter % 2 != 0:
            self.metadata["corruptPacketsDetected"] += 1

        return bits[:-1] # remove the paririty bit

    
    def funcErrorDetectionCRC(self, bits):
        # to be implemented
        # TODO: Tim -> define reciever.CRC
        # decided for CRC-8: http://www.sunshine2k.de/articles/coding/crc/understanding_crc.html#ch4
        generator = "100011101"
        bitlen = len(bits)
        genlen = len(generator) #genlen von CRC-8 ist 9
        curBit = 0
        # nothing = "yield from" # for reference
        bitCalc = list(bits)
        #for i in range(bitlen):
        #    bitCalc += "0"
        while curBit < bitlen-genlen+1:
            #print("".join(bitCalc))
            if bitCalc[curBit] == "1":
                for i in range(genlen):
                    #print(len(bitCalc), curBit+i)
                    bitCalc[curBit+i] = self.XOR(bitCalc[curBit+i], generator[i])
            curBit+=1
            #print(curBit, bitlen)
            #print("".join(bitCalc))
        #for i in range(genlen):
        #    bits += bitCalc[bitlen*2-genlen+i]
        #print("".join(bitCalc))
        for i in range(bitlen):
            if bitCalc[i] != "0":
                self.metadata["corruptPacketsDetected"] += 1
                #print("YOU FUCKED UP!")
                break
        #bitsnew = bits[0:1:bitlen-genlen+1]

        return bits[0:bitlen-genlen+1:1]
        

    def funcErrorCorrectionNone(self, bits):
        """
        Returns input bits without any modification.
        """
        return bits


    def funcErrorCorrectionHammingCode(self, bits):
        """
        Applies hamming code checker and decoder matrix to input bits.
        If checker detects an error the potentially flipped bit is being corrected.
        """
        encodedMessage = np.array([list(bits)], dtype=int)
        syndrome = np.mod(np.dot(self.hamming74checker, encodedMessage.T), 2)
        errorPosition = int("".join(str(syndrome.T[0, i]) for i in range(len(syndrome.T[0])-1, -1, -1)), 2) # 000 --> kein fehler; 001 --> Fehler im ersten Bit; 011 --> Fehler im dritten Bit.
        
        # Fix corrupted bit
        if errorPosition > 0:
            self.metadata["packetsCorrected"] += 1
            if encodedMessage[0][errorPosition - 1] == 1:
                encodedMessage[0][errorPosition - 1] = 0
            else:
                encodedMessage[0][errorPosition - 1] = 1

        return ''.join(str(x[0]) for x in np.dot(self.hamming74decoder, encodedMessage.T))

    @staticmethod
    def XOR(bit1, bit2):
        if (bit1 == bit2):
            return "0"
        else:
            return "1"

    @staticmethod
    def computeHammingDistance(bits1, bits2):
        """
        Computes hamming distance for two 2-bit words.
        """
        hammingDistance = 0
        if bits1[0] is not bits2[0]:
            hammingDistance += 1
        if bits1[1] is not bits2[1]:
            hammingDistance += 1
        return hammingDistance

    def funcErrorCorrectionConvolutionalCode(self, bits):

        self.convCodeCosts = np.full((4, 6), np.inf)
        self.convCodeOrigin = np.ones((4, 6)) * -1
        self.convCodeCosts[0, 0] = 0

        bits = "00" + bits
        # iterate over symbols of two bits at a time
        for idx, i in enumerate(range(0, len(bits), 2)):
            # for every symbol iterate over every possible state
            for row in range(4):
                # for every state iterate over every possible origin state for that particular state
                for origin in self.convCodeStates[row]:
                    # if the current origin state has a cost of < inf calculate the cumulated cost for it
                    if self.convCodeCosts[origin[0], idx] < np.inf:
                        # calculate the hamming distance between the current symbol and the symbol in the state machine if this transition was correct
                        dist = self.computeHammingDistance(bits[i:i+2], origin[1])
                        # if the sum of the calculated cost and the cost of previous hamming distances to get to this point is smaller than the current sum of costs for this state...
                        if self.convCodeCosts[row][idx+1] > dist + self.convCodeCosts[origin[0], idx]:
                            # ...overwrite the larger cost with the new one.
                            self.convCodeCosts[row][idx+1] = dist + self.convCodeCosts[origin[0], idx]
                            # ...track the origin from where the way with the lowest const came.
                            self.convCodeOrigin[row][idx+1] = origin[0] 

        # print(self.convCodeCosts)
        # print("_____")
        # print(self.convCodeOrigin)
        
        #find index of path with smallest total cost
        posFrom = int(np.argmin(self.convCodeCosts[:,-1]))
        
        # generate most likely input message by starting at posFrom and following the computed most likely path through the state machine.
        outBits = ""
        outBitsRaw = ""
        for i in range(5, 1, -1):
            posTo = int(self.convCodeOrigin[posFrom][i])
            outBits = self.convCodeDecoder[posTo][posFrom] + outBits

            ### For metadata only ###
            outBitsRaw = self.convCodeDecoderRaw[posTo][posFrom] + outBitsRaw
            ### ----------------- ###

            posFrom = posTo

        ### For Metadata only ###
        if outBitsRaw != self.bufferEC[:self.param["packetSizeEC"]]:
            self.metadata["packetsCorrected"] += 1
        ### ----------------- ###

        return outBits

def main():
    receiver = Receiver()
    print(receiver.funcErrorCorrectionConvolutionalCode("11011100"))
    #print(receiver.funcErrorDetectionCRC("1100001000001111"))
    
    
if __name__ == "__main__":
    main()
