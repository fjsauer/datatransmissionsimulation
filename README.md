# DataTransmission

## Requirements
- Python version 3.7 or newer

## Installation

1. Open repository folder named ../datatransmission
2. Create new virtual environment using python (cmd: python -m venv .venv)
3. Activate virtual environment (cmd: .venv\Scripts\activate.bat)
4. Install matplotlib (cmd: python -m pip install matplotlib)
6. Run main.py (cmd: python main.py)
